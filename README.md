# "New" Home for Dropzone

I'm sorry to cause any inconvenience, but I decided to move back to GitHub.

**http://github.com/dropzone/dropzone**

The main reasons I switched to GitLab in the first place don't apply anymore
(GitHub has Actions now, which are great, and much better moderation tools), and
there are simply more people used to GitHub.

I know that many of you will be frustrated because you have written issues and
Merge Requests here, but I'm doing my best to get this back on track, and it is
hard to maintain a library this popular by myself.

I've made some much needed changes to Dropzone in the latest versions (`v5.7.5`)
being the latest one, and intend on maintaining it properly again.

Unfortunately I don't have the time or the energy to go through all the MRs and
issues here, so __please__ reopen them on GitHub if you still want your changes
to be implemented.

Thanks for your help, remember not to be an asshole ❤️, and see you back at
GitHub.